#!/bin/bash
#Creating unique key for passwordless authentification

#creating unique keyname adding current date
KeyName="MyKeyPair$(date "+%Y-%m-%d(%H:%M:%S)")"
#Creating a key pair and piping private key directly into a file
aws ec2 create-key-pair --key-name $KeyName --query 'KeyMaterial' --output text > $KeyName.pem
#seting the permissions of private key file so that only we can read it
chmod 400 $KeyName.pem
#Adding private key to the authentication agent
ssh-add -K $KeyName.pem

#Creating VPC
FrontVPC_ID=$(aws ec2 create-vpc --cidr 10.11.12.0/24 --query 'Vpc.VpcId' --output text)
aws ec2 create-tags \
    --resources $FrontVPC_ID \
    --tags Key=Name,Value=FrontVPC
WordPress_SubnetId=$(aws ec2 create-subnet --vpc-id $FrontVPC_ID --cidr-block 10.11.12.0/28 --query 'Subnet.SubnetId' --output text)
aws ec2 create-tags \
    --resources $WordPress_SubnetId \
    --tags Key=Name,Value=WordPress_Subnet
DataBase_SubnetId=$(aws ec2 create-subnet --vpc-id $FrontVPC_ID --cidr-block 10.11.12.32/27 --query 'Subnet.SubnetId' --output text)
aws ec2 create-tags \
    --resources $DataBase_SubnetId \
    --tags Key=Name,Value=DataBase_Subnet
NAT_SubnetId=$(aws ec2 create-subnet --vpc-id $FrontVPC_ID --cidr-block 10.11.12.16/28 --query 'Subnet.SubnetId' --output text)
aws ec2 create-tags \
    --resources $NAT_SubnetId \
    --tags Key=Name,Value=NAT_Subnet
#Creating internet gateway
InternetGatewayId=$(aws ec2 create-internet-gateway --query 'InternetGateway.InternetGatewayId' --output text)
aws ec2 create-tags \
    --resources $InternetGatewayId \
    --tags Key=Name,Value=InternetGateway
#Attaching gateway to VPC
aws ec2 attach-internet-gateway --internet-gateway-id $InternetGatewayId --vpc-id $FrontVPC_ID
#Crating route table
MainRouteTableId=$(aws ec2 create-route-table --vpc-id $FrontVPC_ID --query 'RouteTable.RouteTableId' --output text)
aws ec2 create-tags \
    --resources $MainRouteTableId \
    --tags Key=Name,Value=MainRouteTable
aws ec2 create-route --route-table-id $MainRouteTableId --destination-cidr-block 0.0.0.0/0 --gateway-id $InternetGatewayId
aws ec2 associate-route-table  --subnet-id $NAT_SubnetId --route-table-id $MainRouteTableId
NAT_SecurityGroupID=$(aws ec2 create-security-group --group-name NAT_SecurityGroup --description "NAT security group" --vpc-id $FrontVPC_ID --query 'GroupId' --output text)
aws ec2 create-tags \
    --resources $NAT_SecurityGroupID \
    --tags Key=Name,Value=NAT_SecurityGroup
NAT_InstanceId=$(aws ec2 run-instances \
    --count 1 \
    --image-id ami-00d1f8201864cc10c \
    --instance-type t2.micro \
    --security-group-ids $NAT_SecurityGroupID \
    --subnet-id $NAT_SubnetId \
    --associate-public-ip-address \
    --key-name $KeyName \
    --query 'Instances[].InstanceId' --output text)
aws ec2 modify-instance-attribute \
    --instance-id $NAT_InstanceId\
    --no-source-dest-check
aws ec2 create-tags \
    --resources $NAT_InstanceId \
    --tags Key=Name,Value=NAT_Instance
NAT_PrivateIP=$(aws ec2 describe-instances --instance-id $NAT_InstanceId --query 'Reservations[].Instances[].PrivateIpAddress' --output text)
NAT_PrivateCIDR="${NAT_PrivateIP}/32"
aws ec2 authorize-security-group-ingress \
--group-id $NAT_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp=0.0.0.0/0,Description="SSH Access"}]'
aws ec2 authorize-security-group-ingress \
--group-id $NAT_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=80,ToPort=80,IpRanges='[{CidrIp=10.11.12.0/28,Description="HTTP WP Inbound"}]'
aws ec2 authorize-security-group-ingress \
--group-id $NAT_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=443,ToPort=443,IpRanges='[{CidrIp=10.11.12.0/28,Description="HTTPs WP Inbound"}]'

aws ec2 authorize-security-group-egress \
--group-id $NAT_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=80,ToPort=80,IpRanges='[{CidrIp=0.0.0.0/0,Description="HTTP outbound"}]'
aws ec2 authorize-security-group-egress \
--group-id $NAT_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=443,ToPort=443,IpRanges='[{CidrIp=0.0.0.0/0,Description="HTTPs outbound"}]'
aws ec2 authorize-security-group-egress \
--group-id $NAT_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp=10.11.12.0/28,Description="SSH to WP outbound"}]'
aws ec2 authorize-security-group-egress \
--group-id $NAT_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=22,ToPort=22,IpRanges='[{CidrIp=10.11.12.32/27,Description="SSH to DB outbound"}]'

WordPress_SecurityGroupID=$(aws ec2 create-security-group --group-name WP_SecurityGroup --description "WP security group" --vpc-id $FrontVPC_ID --query 'GroupId' --output text)
aws ec2 create-tags \
    --resources $WordPress_SecurityGroupID \
    --tags Key=Name,Value=WordPress_SecurityGroup
aws ec2 authorize-security-group-ingress \
--group-id $WordPress_SecurityGroupID \
--protocol tcp \
--port 22 \
--cidr $NAT_PrivateCIDR
aws ec2 authorize-security-group-ingress \
--group-id $WordPress_SecurityGroupID \
--protocol tcp \
--port 80 \
--cidr $NAT_PrivateCIDR
aws ec2 authorize-security-group-ingress \
--group-id $WordPress_SecurityGroupID \
--protocol tcp \
--port 443 \
--cidr $NAT_PrivateCIDR
aws ec2 authorize-security-group-ingress \
--group-id $WordPress_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=3306,ToPort=3306,IpRanges='[{CidrIp=10.11.12.32/27,Description="DB Inbound"}]'

aws ec2 authorize-security-group-egress \
--group-id $WordPress_SecurityGroupID \
--protocol tcp \
--port 80 \
--cidr $NAT_PrivateCIDR
aws ec2 authorize-security-group-egress \
--group-id $WordPress_SecurityGroupID \
--protocol tcp \
--port 443 \
--cidr $NAT_PrivateCIDR
aws ec2 authorize-security-group-egress \
--group-id $WordPress_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=3306,ToPort=3306,IpRanges='[{CidrIp=10.11.12.32/27,Description="DB Outbound"}]'
WordPressInstanceId=$(aws ec2 run-instances \
    --count 1 \
    --image-id ami-00d1f8201864cc10c \
    --instance-type t2.micro \
    --security-group-ids $WordPress_SecurityGroupID \
    --subnet-id $WordPress_SubnetId \
    --key-name $KeyName \
    --query 'Instances[].InstanceId' --output text)
aws ec2 create-tags \
    --resources $WordPressInstanceId \
    --tags Key=Name,Value=WordPress_Instance
PrivateRouteTableId=$(aws ec2 create-route-table --vpc-id $FrontVPC_ID --query 'RouteTable.RouteTableId' --output text)
aws ec2 create-tags \
    --resources $PrivateRouteTableId \
    --tags Key=Name,Value=PrivateRouteTable
aws ec2 create-route --route-table-id $PrivateRouteTableId --destination-cidr-block 0.0.0.0/0 --instance-id $NAT_InstanceId
aws ec2 associate-route-table --subnet-id $WordPress_SubnetId --route-table-id $PrivateRouteTableId

DataBase_SecurityGroupID=$(aws ec2 create-security-group --group-name DB_SecurityGroup --description "DB security group" --vpc-id $FrontVPC_ID --query 'GroupId' --output text)
aws ec2 create-tags \
    --resources $DataBase_SecurityGroupID \
    --tags Key=Name,Value=DataBase_SecurityGroup
aws ec2 authorize-security-group-ingress \
--group-id $DataBase_SecurityGroupID \
--protocol tcp \
--port 22 \
--cidr $NAT_PrivateCIDR
aws ec2 authorize-security-group-ingress \
--group-id $DataBase_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=3306,ToPort=3306,IpRanges='[{CidrIp=10.11.12.0/28,Description="WP Inbound"}]'
aws ec2 authorize-security-group-egress \
--group-id $DataBase_SecurityGroupID \
--ip-permissions IpProtocol=tcp,FromPort=3306,ToPort=3306,IpRanges='[{CidrIp=10.11.12.0/28,Description="WP Outbound"}]'
aws ec2 run-instances \
    --count 2 \
    --image-id ami-00d1f8201864cc10c \
    --instance-type t2.micro \
    --security-group-ids $DataBase_SecurityGroupID \
    --subnet-id $DataBase_SubnetId \
    --key-name $KeyName\
    --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=DataBase_Instance}]'


echo 'to enable ssh agent while connecting to NAT Instanse use next commands:
NAT_PBL_IP=$(aws ec2 describe-instances --filters Name=tag:Stack,Values=production --query 'Reservations[].Instances[].PublicIpAddress' --output text)
ssh -A ec2-user@$NAT_PBL_IP
to remove key use:
KeyName=$(aws ec2 describe-instances --filters Name=tag:Stack,Values=production --query 'Reservations[].Instances[].KeyName' --output text)
aws ec2 delete-key-pair --key-name $KeyName
ssh-add -d $KeyName.pe'
