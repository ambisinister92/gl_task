#!/bin/bash
if sudo yum update -y
then
  sudo yum upgrade -y
  sudo yum install -y httpd
  sudo systemctl start httpd
  sudo systemctl enable httpd
  sudo yum install mariadb-server mariadb -y
  sudo systemctl enable mariadb
  sudo systemctl start mariadb
  sudo yum install -y php
  sudo yum install -y php-mysql #not works on redhat
  if [ $? -gt 0 ]
  then
    sudo dnf -y install php php-mysqlnd
  fi
  sudo yum install php-gd -y
  sudo systemctl restart httpd
  sudo su -c 'echo "<?php phpinfo(); ?>">>/var/www/html/info.php'
  sudo sed -i '/^[^#].*/s/^/#/' /etc/httpd/conf.d/welcome.conf
  sudo mysql <<EOF
  CREATE DATABASE wordpress;
  CREATE USER wordpressuser@localhost IDENTIFIED BY 'password';
  GRANT ALL PRIVILEGES ON wordpress.* TO wordpressuser@localhost IDENTIFIED BY 'password';
  FLUSH PRIVILEGES;
EOF
else
  sudo apt update -y
  sudo apt upgrade -y
  sudo apt install apache2 -y
  sudo systemctl start apache2.service
  sudo ufw allow www
  sudo ufw allow https
  sudo apt install mariadb-server -y
  sudo apt install php libapache2-mod-php php-mysql -y
  sudo apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y
  sudo mv /var/www/html/index.html ~/index.html
  sudo systemctl reload apache2
  sudo su -c 'sudo echo "<?php phpinfo(); ?>">/var/www/html/index.php'
  sudo mysql <<EOF
  CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
  GRANT ALL ON wordpress.* TO 'wordpressuser'@'localhost' IDENTIFIED BY 'password';
  FLUSH PRIVILEGES;
EOF
fi
sudo curl -O https://wordpress.org/latest.tar.gz
sudo tar xzvf latest.tar.gz
sudo rsync -avP ~/wordpress/ /var/www/html/
sudo mkdir /var/www/html/wp-content/uploads
sudo chown -R apache:apache /var/www/html/*
sudo cp /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
sudo sed -i "/DB_NAME/c define('DB_NAME', 'wordpress');" /var/www/html/wp-config.php
sudo sed -i "/DB_USER/c define('DB_USER', 'wordpressuser');" /var/www/html/wp-config.php
sudo sed -i "/DB_PASSWORD/c define('DB_PASSWORD', 'password');" /var/www/html/wp-config.php
